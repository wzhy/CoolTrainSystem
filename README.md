Git global setup

git config --global user.name "Wang Zhengyang"
git config --global user.email "wzhyno1@163.com"

Create a new repository

git clone git@gitlab.com:wzhy/CoolTrainSystem.git
cd CoolTrainSystem
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin git@gitlab.com:wzhy/CoolTrainSystem.git
git add .
git commit
git push -u origin master